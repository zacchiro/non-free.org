HOST = $(shell hostname)

LOCAL_CONF = $(wildcard etc/ikiwiki.setup.$(HOST))
ifeq ($(LOCAL_CONF),)
IKIWIKI_CONF = etc/ikiwiki.setup
else
IKIWIKI_CONF = $(LOCAL_CONF)
endif

IKIWIKI_FLAGS = -verbose
IKIWIKI = ikiwiki $(IKIWIKI_FLAGS) -setup $(IKIWIKI_CONF)

all: refresh

refresh:
	$(IKIWIKI) -refresh

rebuild:
	$(IKIWIKI) -rebuild

.PHONY: all refresh rebuild
