function patch_suite() {
    distro = document.getElementById('distrib').value;
    deb_suite = document.getElementById('suite-debian');
    non_free_suite = document.getElementById('suite-non-free');

    deb_suite.innerHTML = distro;
    non_free_suite.innerHTML = distro;
}
